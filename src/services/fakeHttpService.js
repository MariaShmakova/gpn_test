export const reasonsResponse = {
  success: true,
  data: {
    label:
      "Оцените Вашу готовность рекомендовать «Газпромнефть-Корпоративные продажи» своим коллегам / партнерам?",
    reasons: {
      "931": "Задержки документооборота",
      "938": "Неудобный личный кабинет / не хватает опций",
      "492": "Не устраивает тарифная политика",
      "872": "Текущие рабочие вопросы решаются медленно",
      "391": "Неудобная отчетность / нет нужных отчетов",
      "219": "Неудобные / долгие процедуры заключения договора",
      "183": "Сбои при работе с личным кабинетом",
      "252": "Мало дополнительных сервисов для водителей по топливным картам",
      "371": "Не устраивает работа менеджеров",
      "10": "Другое"
    }
  }
};

export function getReasons() {
  return new Promise(resolve => {
    resolve(reasonsResponse);
  });
}

export const http = {
  post: data => {
    console.log("FAKE POST REQUEST: ", data);
  }
};
