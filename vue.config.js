module.exports = {
  configureWebpack: {
    devtool: "none"
  },
  chainWebpack: config => {
    config.module
      .rule("svg")
      .use("file-loader")
      .loader("svg-sprite-loader")
      .options({
        extract: true,
        spriteFilename: "sprite.svg"
      });

    config
      .plugin("svg-sprite-loader-plugin")
      .use(require("svg-sprite-loader/plugin"), [
        {
          plainSprite: true
        }
      ]);
  }
};
